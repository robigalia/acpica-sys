# acpica-sys

[![Crates.io](https://img.shields.io/crates/v/acpica-sys.svg?style=flat-square)](https://crates.io/crates/acpica-sys)

[Documentation](https://doc.robigalia.org/acpica_sys)

Builds acpica, and has bindings to it. To actually use most of it, various
symbols will need to be provided, described in section 9 of the ACPICA
reference manual.

Do note that although this crate is licensed under the MIT or Apache-2.0 at
your option, ACPICA itself is licensed under a custom BSD-like license or
GPL-2.0 at your option.

If you chose to use its BSD-like license, note that the following conditions
must be met:

> 1. Redistributions of source code must retain the above copyright
>    notice, this list of conditions, and the following disclaimer,
>    without modification.
> 2. Redistributions in binary form must reproduce at minimum a disclaimer
>    substantially similar to the "NO WARRANTY" disclaimer below
>    ("Disclaimer") and any redistribution must be conditioned upon
>    including a substantially similar Disclaimer requirement for further
>    binary redistribution.
> 3. Neither the names of the above-listed copyright holders nor the names
>    of any contributors may be used to endorse or promote products derived
>    from this software without specific prior written permission.
> 
> NO WARRANTY
> THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
> "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
> LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTIBILITY AND FITNESS FOR
> A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
> HOLDERS OR CONTRIBUTORS BE LIABLE FOR SPECIAL, EXEMPLARY, OR CONSEQUENTIA
> DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
> OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
> HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
> STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
> IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
> POSSIBILITY OF SUCH DAMAGES.


## Status

Complete, though largely untested.
